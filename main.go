package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"time"

	"gopkg.in/yaml.v2"
)

type yodaQuotes struct {
	Quotes []string `yaml:"quotes"`
}

func readYodaQuotes() (yodaQuotes, error) {
	quotes := yodaQuotes{}

	quotesYaml, err := ioutil.ReadFile("/etc/config/yoda-quotes.yaml")
	if err != nil {
		return quotes, err
	}

	err = yaml.Unmarshal(quotesYaml, &quotes)
	if err != nil {
		return quotes, err
	}

	return quotes, nil
}

func simpleHandler(w http.ResponseWriter, r *http.Request) {
	quotes := []string{
		"The greatest glory in living lies not in never falling, " +
			"but in rising every time we fall.\n- Nelson Mandela",
		"The way to get started is to quit talking and begin doing.\n- Walt Disney",
		"Your time is limited, so don't waste it living someone else's life. " +
			"Don't be trapped by dogma – which is living with the results of other people's thinking.\n- Steve Jobs",
		"If life were predictable it would cease to be life, and be without flavor.\n- Eleanor Roosevelt",
		"If you look at what you have in life, you'll always have more. " +
			"If you look at what you don't have in life, you'll never have enough.\n- Oprah Winfrey",
		"If you set your goals ridiculously high and it's a failure, " +
			"you will fail above everyone else's success.\n- James Cameron",
		"Life is what happens when you're busy making other plans.\n- John Lennon",
	}

	yq, _ := readYodaQuotes()

	quotes = append(quotes, yq.Quotes...)

	rand.Seed(time.Now().UnixNano())
	i := rand.Intn(len(quotes)) //nolint: gosec

	hostname, _ := os.Hostname()

	fmt.Fprintf(w, "Quote from %s\n%s", hostname, quotes[i])
}

func main() {
	http.HandleFunc("/", simpleHandler)
	panic(http.ListenAndServe(":8080", nil))
}
