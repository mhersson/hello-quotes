# README

To mount the yoda-quotes.yaml
```
oc create configmap hello-quotes-yoda --from-file=yoda-quotes.yaml
oc set volume deployment.apps/hello-quotes-git --add -t configmap -m /etc/config --name=hello-quotes-config --configmap-name=hello-quotes-yoda
```
